INSERT INTO user_roles(role)
VALUES ('CLIENT'),
       ('SUPPORT');


INSERT INTO users(login, password, role)
VALUES ('anton', '$argon2id$v=19$m=4096,t=3,p=1$pi1B7DLmROcMBY+nZmHyeg$FIoC4mpuzWJi8ycE4u1gSawnjPEht77A9XixDtJTvJE', 'CLIENT'),
       ('petr', '$argon2id$v=19$m=4096,t=3,p=1$pi1B7DLmROcMBY+nZmHyeg$FIoC4mpuzWJi8ycE4u1gSawnjPEht77A9XixDtJTvJE', 'CLIENT'),
       ('oleg', '$argon2id$v=19$m=4096,t=3,p=1$pi1B7DLmROcMBY+nZmHyeg$FIoC4mpuzWJi8ycE4u1gSawnjPEht77A9XixDtJTvJE', 'SUPPORT');

INSERT INTO tickets_status(status)
VALUES ('OPEN'),
       ('CLOSED');

INSERT INTO tickets (name, content, author)
VALUES ('Printer', 'Printer does not print', 'anton'),
       ('Internet', 'Slow loading web pages', 'petr'),
       ('Email', 'EMail not working', 'petr');


INSERT INTO comments (ticket_id, author, content)
VALUES (2, 'oleg', 'In the external or internal network?'),
       (1, 'oleg', 'Check the connection between the printer and the computer, then restart the computer');
