CREATE TABLE user_roles
(
    role VARCHAR PRIMARY KEY
);

CREATE TABLE users
(
    login    VARCHAR PRIMARY KEY ,
    password VARCHAR NOT NULL,
    role     VARCHAR references user_roles (role)
);

CREATE TABLE tokens
(
    token    VARCHAR PRIMARY KEY ,
    user_login VARCHAR NOT NULL references users(login),
    created timestamptz NOT NULL DEFAULT now()
);


CREATE TABLE tickets_status
(
    status VARCHAR PRIMARY KEY
);

CREATE TABLE tickets
(
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    content TEXT NOT NULL,
    author VARCHAR NOT NULL references users(login),
    created timestamptz NOT NULL DEFAULT now(),
    status VARCHAR references tickets_status(status) NOT NULL DEFAULT 'OPEN'
);

CREATE TABLE comments
(
    id BIGSERIAL PRIMARY KEY,
    content TEXT NOT NULL,
    ticket_id BIGINT NOT NULL references tickets(id),
    author VARCHAR NOT NULL references users(login),
    created timestamptz NOT NULL DEFAULT now()
);


