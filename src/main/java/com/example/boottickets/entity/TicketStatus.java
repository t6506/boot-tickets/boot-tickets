package com.example.boottickets.entity;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum TicketStatus {
  OPEN("OPEN"), CLOSED("CLOSED");

  private final String value;

}
