package com.example.boottickets.entity;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserRole {
  CLIENT("CLIENT"), SUPPORT("SUPPORT");
  private final String value;
}
