package com.example.boottickets.controller;

import com.example.boottickets.dto.TokenRS;
import com.example.boottickets.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping
@RequiredArgsConstructor
public class UserController {
  private final UserService service;

  @GetMapping("/login")
  @PreAuthorize("isAnonymous()")
  public TokenRS login(
      @RequestParam final String login,
      @RequestParam final String password) {
    return service.login(login, password);
  }
}
