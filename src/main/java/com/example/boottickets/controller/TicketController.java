package com.example.boottickets.controller;

import com.example.boottickets.dto.TicketCloseRS;
import com.example.boottickets.dto.TicketCreateRQ;
import com.example.boottickets.dto.TicketCreateRS;
import com.example.boottickets.dto.TicketGetRS;

import com.example.boottickets.service.TicketService;
import com.example.boottickets.security.ApplicationUserDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@Validated
@RestController
@RequestMapping("/tickets")
@RequiredArgsConstructor
public class TicketController {
  private final TicketService ticketService;

  @GetMapping
  public List<TicketGetRS> getAll(
      @AuthenticationPrincipal final ApplicationUserDetails userDetails,
      @RequestParam(defaultValue = "0") final int page) {
    return ticketService.getAll(userDetails);
  }

  @GetMapping("/{ticketId}")
  public TicketGetRS getById(
      @AuthenticationPrincipal final ApplicationUserDetails userDetails,
      @Min(1) @PathVariable("ticketId") final long ticketId) {
    return ticketService.getBYId(userDetails, ticketId);
  }

  @PutMapping("/{ticketId}/close")
  public TicketCloseRS close(
      @AuthenticationPrincipal final ApplicationUserDetails userDetails,
      @Min(1) @PathVariable("ticketId") final long ticketId) {
    return ticketService.close(userDetails, ticketId);
  }

  @PostMapping("/create")
  public TicketCreateRS create(
      @AuthenticationPrincipal ApplicationUserDetails userDetails,
      @Valid @RequestBody final TicketCreateRQ rq) {
    return ticketService.create(userDetails, rq);
  }

}
