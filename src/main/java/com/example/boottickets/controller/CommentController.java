package com.example.boottickets.controller;

import com.example.boottickets.dto.CommentCreateRQ;
import com.example.boottickets.dto.CommentCreateRS;
import com.example.boottickets.service.CommentService;
import com.example.boottickets.security.ApplicationUserDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;


@Validated
@RestController
@RequestMapping("/tickets/{ticketId}/comments")
@RequiredArgsConstructor
public class CommentController {
  private final CommentService commentService;

  @PostMapping("/create")
  public CommentCreateRS create(
      @AuthenticationPrincipal ApplicationUserDetails userDetails,
      @Min(1) @PathVariable final long ticketId,
      @Valid @RequestBody final CommentCreateRQ rq) {
    return commentService.create(userDetails, ticketId, rq);
  }
}
