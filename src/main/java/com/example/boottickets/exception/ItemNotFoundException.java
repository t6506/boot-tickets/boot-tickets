package com.example.boottickets.exception;

public class ItemNotFoundException extends ApplicationException{
  public ItemNotFoundException() {
  }

  public ItemNotFoundException(final String message) {
    super(message);
  }

  public ItemNotFoundException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public ItemNotFoundException(final Throwable cause) {
    super(cause);
  }

  public ItemNotFoundException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
