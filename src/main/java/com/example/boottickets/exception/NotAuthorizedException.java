package com.example.boottickets.exception;

public class NotAuthorizedException extends ApplicationException{
  public NotAuthorizedException() {
  }

  public NotAuthorizedException(final String message) {
    super(message);
  }

  public NotAuthorizedException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public NotAuthorizedException(final Throwable cause) {
    super(cause);
  }

  public NotAuthorizedException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
