package com.example.boottickets.dto;

import com.example.boottickets.entity.TicketStatus;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class TicketCreateRS {
  private long id;
  private String name;
  private String content;
  private OffsetDateTime created;
  private TicketStatus status;
}
