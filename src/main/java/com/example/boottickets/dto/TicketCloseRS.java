package com.example.boottickets.dto;

import com.example.boottickets.entity.TicketStatus;
import lombok.Data;

@Data
public class TicketCloseRS {
  private long id;
  private String name;
  private TicketStatus status;
}
