package com.example.boottickets.dto;

import lombok.Data;

@Data
public class TokenRS {
  private String token;
}
