package com.example.boottickets.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class CommentTicketsByIdDTO {
  private String content;
  private String author;
  private OffsetDateTime created;
}
