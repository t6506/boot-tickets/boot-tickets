package com.example.boottickets.dto;

import lombok.Data;

@Data
public class UserRS {
  private String login;
  private String role;
}
