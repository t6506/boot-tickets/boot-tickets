package com.example.boottickets.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class TicketCreateRQ {
  @NotNull
  private String name;
  @NotNull
  private String content;
}
