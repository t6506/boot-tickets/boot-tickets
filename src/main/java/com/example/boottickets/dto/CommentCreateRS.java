package com.example.boottickets.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class CommentCreateRS {
  private long id;
  private long ticketId;
  private String content;
  private OffsetDateTime created;
}
