package com.example.boottickets.dto;

import com.example.boottickets.entity.TicketStatus;
import lombok.Data;

import java.time.OffsetDateTime;
import java.util.List;

@Data
public class TicketGetRS {
  private long id;
  private String name;
  private String content;
  private OffsetDateTime created;
  private TicketStatus status;
  private List<CommentTicketsByIdDTO> comments;
}
