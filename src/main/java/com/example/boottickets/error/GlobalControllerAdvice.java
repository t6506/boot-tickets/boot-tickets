package com.example.boottickets.error;

import com.example.boottickets.error.dto.ErrorRS;
import com.example.boottickets.exception.ItemNotFoundException;
import com.example.boottickets.exception.NotAuthorizedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.time.OffsetDateTime;

@RestControllerAdvice
public class GlobalControllerAdvice {
  @ExceptionHandler
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  public ErrorRS exception(final NotAuthorizedException e) {
    return new ErrorRS(OffsetDateTime.now(), ErrorRS.UNAUTHORIZED);
  }
  @ExceptionHandler
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ErrorRS exception(final ItemNotFoundException e) {
    return new ErrorRS(OffsetDateTime.now(), ErrorRS.ITEM_NOT_FOUND);
  }

  @ExceptionHandler
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorRS exception(final ConstraintViolationException e) {
    return new ErrorRS(OffsetDateTime.now(), ErrorRS.INCORRECT_REQUEST);
  }





}
