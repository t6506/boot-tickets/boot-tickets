package com.example.boottickets.error.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.OffsetDateTime;

@RequiredArgsConstructor
@Data
public class ErrorRS {
  public static final String UNAUTHORIZED = "err.unauthorized";
  public static final String ITEM_NOT_FOUND = "err.item-not-found";
  public static final String INCORRECT_REQUEST = "err.incorrect-request";

  private final OffsetDateTime timestamp;
  private final String code;
}
