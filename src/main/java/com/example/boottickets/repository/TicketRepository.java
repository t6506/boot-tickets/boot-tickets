package com.example.boottickets.repository;

import com.example.boottickets.entity.TicketEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TicketRepository extends JpaRepository<TicketEntity, Long> {
  List<TicketEntity> findAllByAuthorOrderById(String author);

  List<TicketEntity> findAllByOrderById();
}
