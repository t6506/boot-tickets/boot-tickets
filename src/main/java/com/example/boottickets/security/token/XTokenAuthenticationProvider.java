package com.example.boottickets.security.token;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.util.Assert;

@RequiredArgsConstructor
public class XTokenAuthenticationProvider implements AuthenticationProvider {
  private final UserDetailsChecker userDetailsChecker;
  private final UserDetailsTokenService userDetailsTokenService;

  @Override
  public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
    Assert.isInstanceOf(XTokenAuthenticationToken.class, authentication,
        () -> "Only XTokenAuthenticationToken is supported");
    XTokenAuthenticationToken token = (XTokenAuthenticationToken) authentication;
    String principal = (String) token.getPrincipal();
    UserDetails userDetails = userDetailsTokenService.loadUserByToken(principal);
    userDetailsChecker.check(userDetails);
    XTokenAuthenticationToken result = new XTokenAuthenticationToken(
        userDetails,
        userDetails.getPassword(),
        userDetails.getAuthorities()
    );
    result.setDetails(authentication.getDetails());
    return result;
  }

  @Override
  public boolean supports(final Class<?> authentication) {
    return XTokenAuthenticationToken.class.isAssignableFrom(authentication);
  }
}
