package com.example.boottickets.security.token;

public interface TokenGenerator {
  String generate(final Object principal);
}
