package com.example.boottickets.security.token;

import org.springframework.security.core.AuthenticationException;

public class TokenNotFoundException extends AuthenticationException {
  public TokenNotFoundException(final String msg, final Throwable cause) {
    super(msg, cause);
  }

  public TokenNotFoundException(final String msg) {
    super(msg);
  }
}
