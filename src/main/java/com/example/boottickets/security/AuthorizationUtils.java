package com.example.boottickets.security;

import com.example.boottickets.entity.UserRole;
import com.example.boottickets.entity.TicketEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class AuthorizationUtils {

  public boolean isAuthorOrSupport(final UserDetails userDetails, final TicketEntity ticket) {
    return isSupport(userDetails) || isAuthor(userDetails, ticket);
  }

  public boolean isAuthor(final UserDetails userDetails, final TicketEntity ticket) {
    final String username = userDetails.getUsername();
    final String author = ticket.getAuthor();
    return isClient(userDetails) && username.equals(author);
  }

  public boolean isSupport(final UserDetails userDetails) {
    return getRole(userDetails).equals(UserRole.SUPPORT);
  }

  public boolean isClient(final UserDetails userDetails) {
    return getRole(userDetails).equals(UserRole.CLIENT);
  }

  public UserRole getRole(final UserDetails userDetails) {
    return userDetails.getAuthorities().stream().map(o -> UserRole.valueOf(o.getAuthority())).findFirst().get();
  }
}
