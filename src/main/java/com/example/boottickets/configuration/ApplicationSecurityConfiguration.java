package com.example.boottickets.configuration;

import com.example.boottickets.security.ApplicationUserDetails;
import com.example.boottickets.security.token.XTokenAuthenticationFilter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import java.util.List;

@EnableWebSecurity
@Configuration
public class ApplicationSecurityConfiguration extends WebSecurityConfigurerAdapter {
  @Setter(onMethod_ = @Autowired)
  private List<AuthenticationProvider> providers;

  @Override
  protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
    providers.forEach(auth::authenticationProvider);
  }

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    http
        .sessionManagement()
          .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
          .and()
        .csrf()
          .disable()
        .httpBasic()
          .and()
        .logout()
          .disable()
        .addFilterAfter(new XTokenAuthenticationFilter(
            new WebAuthenticationDetailsSource(), authenticationManager()), BasicAuthenticationFilter.class)
        .anonymous()
        .principal(ApplicationUserDetails.Anonymous.getInstance())
        .and()
        .authorizeRequests()
          .anyRequest()
        .permitAll();
  }
}
