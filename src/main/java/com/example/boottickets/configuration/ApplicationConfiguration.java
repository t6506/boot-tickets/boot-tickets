package com.example.boottickets.configuration;

import com.example.boottickets.security.token.RandomTokenGenerator;
import com.example.boottickets.security.token.TokenGenerator;
import com.example.boottickets.security.token.UserDetailsTokenService;
import com.example.boottickets.security.token.XTokenAuthenticationProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration(proxyBeanMethods = false)
public class ApplicationConfiguration {
  @Bean
  public PasswordEncoder passwordEncoder() {
    return new Argon2PasswordEncoder();
  }

  @Bean
  public TokenGenerator tokenGenerator() {
    return new RandomTokenGenerator();
  }

  @Bean
  public UserDetailsChecker userDetailsChecker() {
    return new AccountStatusUserDetailsChecker();
  }
  @Bean
  public XTokenAuthenticationProvider xTokenAuthenticationProvider(
      final UserDetailsChecker userDetailsChecker,
      final UserDetailsTokenService userDetailsTokenService
      ) {
    return new XTokenAuthenticationProvider(userDetailsChecker, userDetailsTokenService);
  }

  @Bean
  public DaoAuthenticationProvider daoAuthenticationProvider(
      final UserDetailsService userDetailsService,
      final PasswordEncoder passwordEncoder
      ) {
    final DaoAuthenticationProvider bean = new DaoAuthenticationProvider();
    bean.setUserDetailsService(userDetailsService);
    bean.setPasswordEncoder(passwordEncoder);
    //TODO: setUserDetailsPasswordService
    return bean;

  }


}
