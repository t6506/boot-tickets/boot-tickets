package com.example.boottickets.service;

import com.example.boottickets.dto.TicketCloseRS;
import com.example.boottickets.dto.TicketCreateRQ;
import com.example.boottickets.dto.TicketCreateRS;
import com.example.boottickets.dto.TicketGetRS;
import com.example.boottickets.entity.TicketStatus;
import com.example.boottickets.entity.TicketEntity;
import com.example.boottickets.exception.ItemNotFoundException;
import com.example.boottickets.exception.NotAuthorizedException;
import com.example.boottickets.mapper.TicketEntityMapper;
import com.example.boottickets.repository.TicketRepository;
import com.example.boottickets.security.AuthorizationUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class TicketService {
  private final TicketRepository ticketRepository;
  private final TicketEntityMapper mapper;
  private final AuthorizationUtils auth;

  public List<TicketGetRS> getAll(final UserDetails userDetails) {
    final String userName = userDetails.getUsername();
    if (auth.isSupport(userDetails)) {
      return ticketRepository.findAllByOrderById()
          .stream()
          .map(mapper::toTicketGetRS)
          .collect(Collectors.toList());
    }
    if (auth.isClient(userDetails)) {
      return ticketRepository.findAllByAuthorOrderById(userName)
          .stream()
          .map(mapper::toTicketGetRS)
          .collect(Collectors.toList());
    }
    return Collections.emptyList();
  }

  public TicketGetRS getBYId(final UserDetails userDetails, final long ticketId) {
    final String userName = userDetails.getUsername();
    final TicketEntity ticket = ticketRepository.findById(ticketId)
        .orElseThrow(() -> new ItemNotFoundException(String.valueOf(ticketId)));
    if (!auth.isAuthorOrSupport(userDetails, ticket)) {
      throw new NotAuthorizedException(userName);
    }
      return mapper.toTicketGetRS(ticket);
  }

  public TicketCloseRS close(final UserDetails userDetails, final long ticketId) {
    final TicketEntity ticket = ticketRepository.findById(ticketId)
        .orElseThrow(() -> new ItemNotFoundException(String.valueOf(ticketId)));
    if (!auth.isAuthor(userDetails, ticket)) {
      throw new NotAuthorizedException(userDetails.getUsername());
    }
    ticket.setStatus(TicketStatus.CLOSED);
    return mapper.toTicketCloseRS(ticket);
  }

  public TicketCreateRS create(final UserDetails userDetails, final TicketCreateRQ rq) {
    if (!auth.isClient(userDetails)) {
      throw new NotAuthorizedException(userDetails.getUsername());
    }
    final TicketEntity saved = ticketRepository.save(mapper.fromTicketCreateRQ(userDetails, rq, OffsetDateTime.now(), TicketStatus.OPEN));
    return mapper.toTicketCreateRS(saved);
  }
}
