package com.example.boottickets.service;

import com.example.boottickets.dto.UserRS;
import com.example.boottickets.dto.TokenRS;
import com.example.boottickets.entity.TokenEntity;
import com.example.boottickets.entity.UserEntity;
import com.example.boottickets.mapper.TokenEntityMapper;
import com.example.boottickets.mapper.UserEntityMapper;
import com.example.boottickets.repository.TokenRepository;
import com.example.boottickets.repository.UserRepository;
import com.example.boottickets.security.ApplicationUserDetails;
import com.example.boottickets.security.token.TokenGenerator;
import com.example.boottickets.security.token.TokenNotFoundException;
import com.example.boottickets.security.token.UserDetailsTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class UserService implements UserDetailsService, InitializingBean, UserDetailsTokenService {
  public static final String SORT_PROPERTY = "login";
  private final UserRepository userRepository;
  private final UserEntityMapper userEntityMapper;
  private final PasswordEncoder passwordEncoder;
  private final TokenGenerator tokenGenerator;
  private final TokenRepository tokenRepository;
  private final TokenEntityMapper tokenEntityMapper;

  private UserEntity notFoundUserEntity;

  @Override
  public void afterPropertiesSet() throws Exception {
    notFoundUserEntity = new UserEntity("notfound",  passwordEncoder.encode("dummy password"), null);
  }

  @Override
  public UserDetails loadUserByUsername(final String login) throws UsernameNotFoundException {
    final UserEntity entity = userRepository.findById(login).orElseThrow(() -> new UsernameNotFoundException(login));
    return userEntityMapper.toUserDetails(entity);
  }

  @Override
  public UserDetails loadUserByToken(final String token) {
    final TokenEntity saved = tokenRepository.findById(token).orElseThrow(() -> new TokenNotFoundException(token));
    return tokenEntityMapper.toUserDetails(saved);
  }

  public List<UserRS> getAll(final int page) {
    final Page<UserEntity> entities = userRepository.findAll(PageRequest.of(page, 1, Sort.Direction.ASC, SORT_PROPERTY));
    return userEntityMapper.toListRS(entities.toList());
  }

  @Transactional(propagation = Propagation.NEVER)
  public TokenRS login(final String login, final String password) {
    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    final UserEntity savedUser = userRepository.findById(login).orElse(notFoundUserEntity);
    if (!passwordEncoder.matches(password, savedUser.getPassword())) {
      throw new BadCredentialsException("bad credentials");
    }
    if (savedUser.equals(notFoundUserEntity)) {
      throw new BadCredentialsException("bad credentials");
    }
    final ApplicationUserDetails principal = userEntityMapper.toUserDetails(savedUser);
    final String token = tokenGenerator.generate(principal);
    final TokenEntity savedToken = tokenRepository.save(new TokenEntity(token, savedUser));
    return tokenEntityMapper.toRS(savedToken);
  }


}
