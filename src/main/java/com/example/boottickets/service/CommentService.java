package com.example.boottickets.service;

import com.example.boottickets.dto.CommentCreateRQ;
import com.example.boottickets.dto.CommentCreateRS;
import com.example.boottickets.entity.CommentEntity;
import com.example.boottickets.entity.TicketEntity;
import com.example.boottickets.exception.ItemNotFoundException;
import com.example.boottickets.exception.NotAuthorizedException;
import com.example.boottickets.mapper.CommentEntityMapper;
import com.example.boottickets.repository.CommentRepository;
import com.example.boottickets.repository.TicketRepository;
import com.example.boottickets.security.AuthorizationUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;

@Service
@Transactional
@RequiredArgsConstructor
public class CommentService {
  private final CommentRepository commentRepository;
  private final CommentEntityMapper mapper;
  private final TicketRepository ticketRepository;
  private final AuthorizationUtils auth;

  public CommentCreateRS create(final UserDetails userDetails, final long ticketId, final CommentCreateRQ rq) {
    final String userName = userDetails.getUsername();
    final TicketEntity ticket = ticketRepository.findById(ticketId).orElseThrow(() -> new ItemNotFoundException(String.valueOf(ticketId)));
    if (!auth.isAuthorOrSupport(userDetails, ticket)) {
      throw new NotAuthorizedException(userName);
    }
    final CommentEntity comment = mapper.fromCommentCreateRQ(userName, ticket, rq, OffsetDateTime.now());
    return mapper.toCommentCreateRS(commentRepository.save(comment));
  }
}


