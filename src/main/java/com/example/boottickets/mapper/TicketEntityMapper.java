package com.example.boottickets.mapper;

import com.example.boottickets.dto.TicketCloseRS;
import com.example.boottickets.dto.TicketCreateRQ;
import com.example.boottickets.dto.TicketCreateRS;
import com.example.boottickets.dto.TicketGetRS;
import com.example.boottickets.entity.TicketStatus;
import com.example.boottickets.entity.TicketEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Mapper
@Service
public interface TicketEntityMapper {

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "name", source = "rq.name")
  @Mapping(target = "content", source = "rq.content")
  @Mapping(target = "author", source = "userDetails.username")
  @Mapping(target = "created", source = "created")
  @Mapping(target = "status", source = "status")
  @Mapping(target = "comments", ignore = true)
  TicketEntity fromTicketCreateRQ(final UserDetails userDetails,
                                  final TicketCreateRQ rq,
                                  final OffsetDateTime created,
                                  final TicketStatus status);

  TicketCreateRS toTicketCreateRS(TicketEntity entity);

  TicketCloseRS toTicketCloseRS(TicketEntity entity);

  TicketGetRS toTicketGetRS(TicketEntity entity);
}
