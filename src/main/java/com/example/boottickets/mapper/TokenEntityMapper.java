package com.example.boottickets.mapper;

import com.example.boottickets.dto.TokenRS;
import com.example.boottickets.entity.UserRole;
import com.example.boottickets.entity.TokenEntity;
import com.example.boottickets.security.ApplicationUserDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;


@Mapper
public interface TokenEntityMapper {
  TokenRS toRS(final TokenEntity entity);

  @Mapping(target = "username", source = "user.login")
  @Mapping(target = "password", source = "user.password")
  @Mapping(target = "authorities", source = "user.role")
  @Mapping(target = "accountNonExpired", ignore = true)
  @Mapping(target = "accountNonLocked", ignore = true)
  @Mapping(target = "credentialsNonExpired", ignore = true)
  @Mapping(target = "enabled", ignore = true)
  ApplicationUserDetails toUserDetails(TokenEntity saved);

  default Collection<? extends GrantedAuthority> map(UserRole role) {
    return Collections.singleton(new SimpleGrantedAuthority(role.name()));
  }
}
