package com.example.boottickets.mapper;

import com.example.boottickets.dto.UserRS;
import com.example.boottickets.entity.UserRole;
import com.example.boottickets.entity.UserEntity;
import com.example.boottickets.security.ApplicationUserDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Mapper
public interface UserEntityMapper {

  List<UserRS> toListRS(final List<UserEntity> entities);

  @Mapping(target = "username", source = "login")
  @Mapping(target = "authorities", source = "role")
  @Mapping(target = "accountNonExpired", ignore = true)
  @Mapping(target = "accountNonLocked", ignore = true)
  @Mapping(target = "credentialsNonExpired", ignore = true)
  @Mapping(target = "enabled", ignore = true)
  ApplicationUserDetails toUserDetails(final UserEntity entity);

  default Collection<? extends GrantedAuthority> map(UserRole role) {
    return Collections.singleton(new SimpleGrantedAuthority(role.name()));
  }
}
