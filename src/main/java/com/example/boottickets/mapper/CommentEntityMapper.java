package com.example.boottickets.mapper;

import com.example.boottickets.dto.CommentCreateRQ;
import com.example.boottickets.dto.CommentCreateRS;
import com.example.boottickets.dto.CommentTicketsByIdDTO;
import com.example.boottickets.entity.CommentEntity;
import com.example.boottickets.entity.TicketEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Mapper
@Service
public interface CommentEntityMapper {

  CommentTicketsByIdDTO toCommentTicketsByIdDTO(CommentEntity entity);


  @Mapping(target = "id", ignore = true)
  @Mapping(target = "ticket", source = "ticket")
  @Mapping(target = "content", source = "rq.content")
  @Mapping(target = "author", source = "userName")
  @Mapping(target = "created", source = "created")
  CommentEntity fromCommentCreateRQ(final String userName,
                                    final TicketEntity ticket,
                                    final CommentCreateRQ rq,
                                    final OffsetDateTime created);

  @Mapping(target = "ticketId", source = "ticket.id")
  CommentCreateRS toCommentCreateRS(CommentEntity entity);
}
